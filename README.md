# README #

This program uses the GetICMProfile, WcsGetDefaultColorProfile, and ColorProfileGetDisplayDefault functions to get the default profile of the monitor and display the file name.

モニタの既定のプロファイルを取得してファイル名を表示します。従来の方法（GetICMProfile()）と新しい方法（ColorProfileGetDisplayDefault()）とで挙動の違いを比較できます。

### Usage / 使用方法 ###

Move the window to the target monitor and then click the Update button to get the profile for that monitor.

MonitorProfileSample.exeを起動すると、ウインドウが現れた側のモニタの既定のプロファイルを取得して表示します。「更新」ボタンをクリックすると再取得を行います。

マルチモニタ環境では、プロファイルを取得したい対象のモニタ領域にウインドウを移動させてボタンをクリックすれば、該当モニタのプロファイルを取得できます。
