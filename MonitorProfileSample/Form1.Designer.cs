﻿namespace MonitorProfileSample
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_hMonitor = new System.Windows.Forms.Label();
            this.label_devName = new System.Windows.Forms.Label();
            this.label_targetAdapterID = new System.Windows.Forms.Label();
            this.label_sourceID = new System.Windows.Forms.Label();
            this.textBox_old = new System.Windows.Forms.TextBox();
            this.textBox_new = new System.Windows.Forms.TextBox();
            this.textBox_wcs = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label9, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label_hMonitor, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_devName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_targetAdapterID, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label_sourceID, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox_old, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox_new, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox_wcs, 1, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(14, 12);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(857, 407);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(254, 135);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(599, 45);
            this.label9.TabIndex = 17;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(4, 360);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(242, 45);
            this.label8.TabIndex = 16;
            this.label8.Text = "ColorProfileGetDisplayDefault()";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(4, 270);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(242, 45);
            this.label7.TabIndex = 15;
            this.label7.Text = "GetICMProfile()";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.SetColumnSpan(this.button1, 2);
            this.button1.Location = new System.Drawing.Point(4, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(849, 37);
            this.button1.TabIndex = 0;
            this.button1.Text = "更新";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "モニタ ハンドル";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 45);
            this.label2.TabIndex = 2;
            this.label2.Text = "GDIデバイス名";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(242, 45);
            this.label3.TabIndex = 3;
            this.label3.Text = "デバイス名";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(242, 45);
            this.label4.TabIndex = 4;
            this.label4.Text = "targetAdapterID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 225);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(242, 45);
            this.label5.TabIndex = 5;
            this.label5.Text = "sourceID";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(4, 315);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(242, 45);
            this.label6.TabIndex = 7;
            this.label6.Text = "WcsGetDefaultColorProfile()";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_hMonitor
            // 
            this.label_hMonitor.AutoSize = true;
            this.label_hMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_hMonitor.Location = new System.Drawing.Point(254, 45);
            this.label_hMonitor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_hMonitor.Name = "label_hMonitor";
            this.label_hMonitor.Size = new System.Drawing.Size(599, 45);
            this.label_hMonitor.TabIndex = 8;
            this.label_hMonitor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_devName
            // 
            this.label_devName.AutoSize = true;
            this.label_devName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_devName.Location = new System.Drawing.Point(254, 90);
            this.label_devName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_devName.Name = "label_devName";
            this.label_devName.Size = new System.Drawing.Size(599, 45);
            this.label_devName.TabIndex = 9;
            this.label_devName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_targetAdapterID
            // 
            this.label_targetAdapterID.AutoSize = true;
            this.label_targetAdapterID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_targetAdapterID.Location = new System.Drawing.Point(254, 180);
            this.label_targetAdapterID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_targetAdapterID.Name = "label_targetAdapterID";
            this.label_targetAdapterID.Size = new System.Drawing.Size(599, 45);
            this.label_targetAdapterID.TabIndex = 10;
            this.label_targetAdapterID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_sourceID
            // 
            this.label_sourceID.AutoSize = true;
            this.label_sourceID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_sourceID.Location = new System.Drawing.Point(254, 225);
            this.label_sourceID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_sourceID.Name = "label_sourceID";
            this.label_sourceID.Size = new System.Drawing.Size(599, 45);
            this.label_sourceID.TabIndex = 11;
            this.label_sourceID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox_old
            // 
            this.textBox_old.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_old.Location = new System.Drawing.Point(254, 278);
            this.textBox_old.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.textBox_old.Name = "textBox_old";
            this.textBox_old.ReadOnly = true;
            this.textBox_old.Size = new System.Drawing.Size(599, 25);
            this.textBox_old.TabIndex = 12;
            // 
            // textBox_new
            // 
            this.textBox_new.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_new.Location = new System.Drawing.Point(254, 368);
            this.textBox_new.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.textBox_new.Name = "textBox_new";
            this.textBox_new.ReadOnly = true;
            this.textBox_new.Size = new System.Drawing.Size(599, 25);
            this.textBox_new.TabIndex = 14;
            // 
            // textBox_wcs
            // 
            this.textBox_wcs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_wcs.Location = new System.Drawing.Point(254, 323);
            this.textBox_wcs.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.textBox_wcs.Name = "textBox_wcs";
            this.textBox_wcs.ReadOnly = true;
            this.textBox_wcs.Size = new System.Drawing.Size(599, 25);
            this.textBox_wcs.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(885, 431);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(712, 362);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(14, 12, 14, 12);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitor profile";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_hMonitor;
        private System.Windows.Forms.Label label_devName;
        private System.Windows.Forms.Label label_targetAdapterID;
        private System.Windows.Forms.Label label_sourceID;
        private System.Windows.Forms.TextBox textBox_old;
        private System.Windows.Forms.TextBox textBox_new;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_wcs;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}

