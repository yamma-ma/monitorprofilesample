﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Win32;
using MonitorProfileSample.Properties;

namespace MonitorProfileSample
{
    public partial class Form1 : Form
    {
        bool readyWcsAPI = false;
        bool readyNewAPI = false;

        public Form1()
        {
            InitializeComponent();

            button1.Text = Resources.UpdateButtonText;
            label1.Text = Resources.MonitorHandle;
            label2.Text = Resources.GDIDeviceName;
            label3.Text = Resources.DeviceName;

            #region Check WCS API
            try
            {
                bool dummy;
                NativeMethods.WcsGetUsePerUserProfiles("", DeviceClass.CLASS_MONITOR, out dummy);
                readyWcsAPI = true;
            }
            catch
            {
                textBox_wcs.Text = "n/a";
            }
            #endregion

            #region Check RS5 new API
            try
            {
                WCS_PROFILE_MANAGEMENT_SCOPE dummy;
                NativeMethods.ColorProfileGetDisplayUserScope(new LUID(), 0, out dummy);
                readyNewAPI = true;
            }
            catch
            {
                textBox_new.Text = "n/a";
            }
            #endregion

            button1.Click += (sender, e) =>
            {
                var hMonitor = NativeMethods.MonitorFromWindow(Handle, MONITORFROMWINDOWFLAGS.MONITOR_DEFAULTTONEAREST);
                label_hMonitor.Text = $"{(uint)hMonitor,8:X8}";

                var info = new MONITORINFOEX();
                info.cbSize = (uint)Marshal.SizeOf(info);

                if (NativeMethods.GetMonitorInfo(hMonitor, ref info))
                {
                    WCS_PROFILE_MANAGEMENT_SCOPE scope;
                    string profilePath = "n/a";

                    label_devName.Text = info.szDevice;

                    #region GetICMProfile
                    var len = 2048;
                    var builder = new StringBuilder(len);

                    var hDC = NativeMethods.CreateIC("MONITOR", info.szDevice, IntPtr.Zero, IntPtr.Zero);
                    if (NativeMethods.GetICMProfile(hDC, out len, builder))
                    {
                        textBox_old.Text = builder.ToString();
                    }
                    else
                    {
                        textBox_old.Text = "n/a";
                    }

                    if (hDC != IntPtr.Zero)
                    {
                        NativeMethods.DeleteDC(hDC);
                    }
                    #endregion

                    #region WcsGetDefaultColorProfile
                    var dDev = new DISPLAY_DEVICE();
                    dDev.cb = Marshal.SizeOf(dDev);
                    bool result;

                    profilePath = "n/a";

                    if ((result = NativeMethods.EnumDisplayDevices(info.szDevice, 0, ref dDev, 0)))
                    {
                        label9.Text = dDev.DeviceKey;
                    }
                    else
                    {
                        label9.Text = "n/a";
                    }

                    if (readyWcsAPI && result)
                    {
                        bool usePerUserProfiles = false;
                        NativeMethods.WcsGetUsePerUserProfiles(dDev.DeviceKey, DeviceClass.CLASS_MONITOR, out usePerUserProfiles);

                        scope = usePerUserProfiles ? WCS_PROFILE_MANAGEMENT_SCOPE.WCS_PROFILE_MANAGEMENT_SCOPE_CURRENT_USER : WCS_PROFILE_MANAGEMENT_SCOPE.WCS_PROFILE_MANAGEMENT_SCOPE_SYSTEM_WIDE;

                        UInt32 size = 0;

                        if (NativeMethods.WcsGetDefaultColorProfileSize(scope, dDev.DeviceKey, COLORPROFILETYPE.CPT_ICC, COLORPROFILESUBTYPE.CPST_NONE, 0, out size))
                        {
                            var sPtr = Marshal.AllocCoTaskMem((int)size);

                            if (NativeMethods.WcsGetDefaultColorProfile(scope, dDev.DeviceKey, COLORPROFILETYPE.CPT_ICC, COLORPROFILESUBTYPE.CPST_NONE, 0, size, sPtr))
                            {
                                profilePath = Marshal.PtrToStringUni(sPtr);
                            }

                            Marshal.FreeCoTaskMem(sPtr);
                        }
                    }

                    textBox_wcs.Text = profilePath;
                    #endregion

                    #region ColorProfileGetDisplayDefault
                    profilePath = "n/a";

                    try
                    {
                        var pathInfo = new DISPLAYCONFIG_PATH_INFO(info.szDevice);
                        label_targetAdapterID.Text = $"{pathInfo.sourceInfo.adapterId}";
                        label_sourceID.Text = $"{pathInfo.sourceInfo.id}";

                        if (readyNewAPI)
                        {
                            try
                            {
                                IntPtr sPtr = IntPtr.Zero;

                                NativeMethods.ColorProfileGetDisplayUserScope(pathInfo.sourceInfo.adapterId, pathInfo.sourceInfo.id, out scope);

                                if (NativeMethods.ColorProfileGetDisplayDefault(scope, pathInfo.sourceInfo.adapterId, pathInfo.sourceInfo.id, COLORPROFILETYPE.CPT_ICC, COLORPROFILESUBTYPE.CPST_NONE, out sPtr) == 0)
                                {
                                    profilePath = Marshal.PtrToStringUni(sPtr);
                                    NativeMethods.LocalFree(sPtr);

                                    #region 高度なカラーのプロファイルを取得
                                    if (NativeMethods.ColorProfileGetDisplayDefault(scope, pathInfo.sourceInfo.adapterId, pathInfo.sourceInfo.id, COLORPROFILETYPE.CPT_ICC, COLORPROFILESUBTYPE.CPST_EXTENDED_DISPLAY_COLOR_MODE, out sPtr) == 0)
                                    {
                                        var tmp = Marshal.PtrToStringUni(sPtr);
                                        NativeMethods.LocalFree(sPtr);

                                        if (profilePath != tmp)
                                        {
                                            profilePath = $"{profilePath} / {tmp}（{Resources.AdvancedColor}）";
                                        }
                                    }
                                    #endregion
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    catch
                    {
                        label_targetAdapterID.Text = label_sourceID.Text = "n/a";
                    }

                    textBox_new.Text = profilePath;
                    #endregion
                }
                else
                {
                    label_devName.Text = label_targetAdapterID.Text = label_sourceID.Text = textBox_old.Text = textBox_new.Text = "n/a";
                }
            };

            Load += (sender, e) => button1.PerformClick();
        }
    }
}
