﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Win32
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LUID
    {
        UInt32 LowPart;
        Int32 HighPart;

        public override string ToString()
        {
            Int64 tmp = (Int64)HighPart << 32;
            tmp |= LowPart;
            return tmp.ToString("X16");
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct POINTL
    {
        int x;
        int y;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RECTL
    {
        int left;
        int top;
        int right;
        int bottom;
    }

    enum MONITORFROMWINDOWFLAGS : uint
    {
        MONITOR_DEFAULTTONULL = 0,
        MONITOR_DEFAULTTOPRIMARY = 1,
        MONITOR_DEFAULTTONEAREST = 2
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct MONITORINFOEX
    {
        public uint cbSize;
        public RECT rcMonitor;
        public RECT rcWork;
        public uint dwFlags;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string szDevice; //CCHDEVICENAME
    }

    #region wingdi.h
    [Flags]
    public enum DisplayDeviceStateFlags
    {
        DISPLAY_DEVICE_ATTACHED_TO_DESKTOP = 0x00000001,
        DISPLAY_DEVICE_MULTI_DRIVER        = 0x00000002,
        DISPLAY_DEVICE_PRIMARY_DEVICE      = 0x00000004,
        DISPLAY_DEVICE_MIRRORING_DRIVER    = 0x00000008,
        DISPLAY_DEVICE_VGA_COMPATIBLE      = 0x00000010,
        DISPLAY_DEVICE_REMOVABLE           = 0x00000020,
        DISPLAY_DEVICE_ACC_DRIVER          = 0x00000040,
        DISPLAY_DEVICE_MODESPRUNED         = 0x08000000,
        DISPLAY_DEVICE_RDPUDD              = 0x01000000,
        DISPLAY_DEVICE_REMOTE              = 0x04000000,
        DISPLAY_DEVICE_DISCONNECT          = 0x02000000,
        DISPLAY_DEVICE_TS_COMPATIBLE       = 0x00200000,
        DISPLAY_DEVICE_UNSAFE_MODES_ON     = 0x00080000,
        /* Child device state */
        DISPLAY_DEVICE_ACTIVE              = 0x00000001,
        DISPLAY_DEVICE_ATTACHED            = 0x00000002
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct DISPLAY_DEVICE
    {
        public int cb;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string DeviceName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string DeviceString;
        public DisplayDeviceStateFlags StateFlags;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string DeviceID;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string DeviceKey;
    }

    public enum DISPLAYCONFIG_VIDEO_OUTPUT_TECHNOLOGY
    {
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_OTHER = -1,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HD15 = 0,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_SVIDEO = 1,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_COMPOSITE_VIDEO = 2,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_COMPONENT_VIDEO = 3,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DVI = 4,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HDMI = 5,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_LVDS = 6,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_D_JPN = 8,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_SDI = 9,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DISPLAYPORT_EXTERNAL = 10,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DISPLAYPORT_EMBEDDED = 11,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_UDI_EXTERNAL = 12,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_UDI_EMBEDDED = 13,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_SDTVDONGLE = 14,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_MIRACAST = 15,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_INDIRECT_WIRED = 16,
        DISPLAYCONFIG_OUTPUT_TECHNOLOGY_INDIRECT_VIRTUAL = 17
        // DISPLAYCONFIG_OUTPUT_TECHNOLOGY_INTERNAL = 0x80000000,
        // DISPLAYCONFIG_OUTPUT_TECHNOLOGY_FORCE_UINT32 = 0xFFFFFFFF
    }

    public enum DISPLAYCONFIG_ROTATION
    {
        DISPLAYCONFIG_ROTATION_IDENTITY = 1,
        DISPLAYCONFIG_ROTATION_ROTATE90 = 2,
        DISPLAYCONFIG_ROTATION_ROTATE180 = 3,
        DISPLAYCONFIG_ROTATION_ROTATE270 = 4
        // DISPLAYCONFIG_ROTATION_FORCE_UINT32 = 0xFFFFFFFF
    }

    public enum DISPLAYCONFIG_SCALING
    {
        DISPLAYCONFIG_SCALING_IDENTITY = 1,
        DISPLAYCONFIG_SCALING_CENTERED = 2,
        DISPLAYCONFIG_SCALING_STRETCHED = 3,
        DISPLAYCONFIG_SCALING_ASPECTRATIOCENTEREDMAX = 4,
        DISPLAYCONFIG_SCALING_CUSTOM = 5,
        DISPLAYCONFIG_SCALING_PREFERRED = 128
        //DISPLAYCONFIG_SCALING_FORCE_UINT32 = 0xFFFFFFFF
    }

    public enum DISPLAYCONFIG_SCANLINE_ORDERING
    {
        DISPLAYCONFIG_SCANLINE_ORDERING_UNSPECIFIED = 0,
        DISPLAYCONFIG_SCANLINE_ORDERING_PROGRESSIVE = 1,
        DISPLAYCONFIG_SCANLINE_ORDERING_INTERLACED = 2,
        DISPLAYCONFIG_SCANLINE_ORDERING_INTERLACED_UPPERFIELDFIRST = DISPLAYCONFIG_SCANLINE_ORDERING_INTERLACED,
        DISPLAYCONFIG_SCANLINE_ORDERING_INTERLACED_LOWERFIELDFIRST = 3
        // DISPLAYCONFIG_SCANLINE_ORDERING_FORCE_UINT32 = 0xFFFFFFFF
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_RATIONAL
    {
        public UInt32 Numerator;
        public UInt32 Denominator;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_PATH_SOURCE_INFO
    {
        public LUID adapterId;
        public UInt32 id;
        public UInt32 modeInfoIdx; // UINT32 cloneGroupId : 16; UINT32 sourceModeInfoIdx : 16;
        public UInt32 statusFlags;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_PATH_TARGET_INFO
    {
        public LUID adapterId;
        public UInt32 id;
        public UInt32 modeInfoIdx; // UINT32 desktopModeInfoIdx : 16; UINT32 targetModeInfoIdx : 16;
        public DISPLAYCONFIG_VIDEO_OUTPUT_TECHNOLOGY outputTechnology;
        public DISPLAYCONFIG_ROTATION rotation;
        public DISPLAYCONFIG_SCALING scaling;
        public DISPLAYCONFIG_RATIONAL refreshRate;
        public DISPLAYCONFIG_SCANLINE_ORDERING scanLineOrdering;
        public bool targetAvailable;
        public UInt32 statusFlags;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_PATH_INFO
    {
        public DISPLAYCONFIG_PATH_SOURCE_INFO sourceInfo;
        public DISPLAYCONFIG_PATH_TARGET_INFO targetInfo;
        public UInt32 flags;

        public DISPLAYCONFIG_PATH_INFO(string monitorName)
        {
            int result = 0;

            UInt32 NumPathArrayElements = 0;
            UInt32 NumModeInfoArrayElements = 0;
            DISPLAYCONFIG_PATH_INFO[] PathInfoArray = null;
            DISPLAYCONFIG_MODE_INFO[] ModeInfoArray = null;

            do
            {
                PathInfoArray = null;
                ModeInfoArray = null;

                result = NativeMethods.GetDisplayConfigBufferSizes(QDCFlags.QDC_ONLY_ACTIVE_PATHS, out NumPathArrayElements, out NumModeInfoArrayElements);
                if (result != 0)
                {
                    throw new Exception("GetDisplayConfigBufferSizes()の呼び出しに失敗");
                }

                PathInfoArray = new DISPLAYCONFIG_PATH_INFO[NumPathArrayElements];
                ModeInfoArray = new DISPLAYCONFIG_MODE_INFO[NumModeInfoArrayElements];

                result = NativeMethods.QueryDisplayConfig(QDCFlags.QDC_ONLY_ACTIVE_PATHS, ref NumPathArrayElements, PathInfoArray, ref NumModeInfoArrayElements, ModeInfoArray, IntPtr.Zero);
            }
            while (result == 122); // ERROR_INSUFFICIENT_BUFFER

            int DesiredPathIdx = -1;

            if (result == 0)
            {
                DISPLAYCONFIG_SOURCE_DEVICE_NAME SourceName = new DISPLAYCONFIG_SOURCE_DEVICE_NAME();

                for (int pathIdx = 0; pathIdx < NumPathArrayElements; pathIdx++)
                {
                    SourceName.header.type = DISPLAYCONFIG_DEVICE_INFO_TYPE.DISPLAYCONFIG_DEVICE_INFO_GET_SOURCE_NAME;
                    SourceName.header.size = (uint)Marshal.SizeOf(SourceName);
                    SourceName.header.adapterId = PathInfoArray[pathIdx].sourceInfo.adapterId;
                    SourceName.header.id = PathInfoArray[pathIdx].sourceInfo.id;

                    result = NativeMethods.DisplayConfigGetDeviceInfo(ref SourceName);

                    if (result == 0)
                    {
                        if (monitorName == SourceName.viewGdiDeviceName)
                        {
                            DesiredPathIdx = pathIdx;
                        }
                    }
                }
            }

            if (DesiredPathIdx != -1)
            {
                sourceInfo = PathInfoArray[DesiredPathIdx].sourceInfo;
                targetInfo = PathInfoArray[DesiredPathIdx].targetInfo;
                flags = PathInfoArray[DesiredPathIdx].flags;
            }
            else
            {
                throw new ArgumentException("指定のモニタは見つかりませんでした。");
            }
        }
    }

    public enum DISPLAYCONFIG_MODE_INFO_TYPE
    {
        DISPLAYCONFIG_MODE_INFO_TYPE_SOURCE = 1,
        DISPLAYCONFIG_MODE_INFO_TYPE_TARGET = 2,
        DISPLAYCONFIG_MODE_INFO_TYPE_DESKTOP_IMAGE = 3
        //DISPLAYCONFIG_MODE_INFO_TYPE_FORCE_UINT32 = 0xFFFFFFFF
    }

    public enum DISPLAYCONFIG_PIXELFORMAT
    {
        DISPLAYCONFIG_PIXELFORMAT_8BPP = 1,
        DISPLAYCONFIG_PIXELFORMAT_16BPP = 2,
        DISPLAYCONFIG_PIXELFORMAT_24BPP = 3,
        DISPLAYCONFIG_PIXELFORMAT_32BPP = 4,
        DISPLAYCONFIG_PIXELFORMAT_NONGDI = 5
        // DISPLAYCONFIG_PIXELFORMAT_FORCE_UINT32 = 0xffffffff
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_2DREGION
    {
        public UInt32 cx;
        public UInt32 cy;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_VIDEO_SIGNAL_INFO
    {
        public UInt64 pixelRate;
        public DISPLAYCONFIG_RATIONAL hSyncFreq;
        public DISPLAYCONFIG_RATIONAL vSyncFreq;
        public DISPLAYCONFIG_2DREGION activeSize;
        public DISPLAYCONFIG_2DREGION totalSize;
        public UInt32 AdditionalSignalInfo; // (videoStandard : 16; vSyncFreqDivider : 6; reserved : 10;) | UINT32 videoStandard;
        public DISPLAYCONFIG_SCANLINE_ORDERING scanLineOrdering;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_SOURCE_MODE
    {
        public UInt32 width;
        public UInt32 height;
        public DISPLAYCONFIG_PIXELFORMAT pixelFormat;
        public POINTL position;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_TARGET_MODE
    {
        public DISPLAYCONFIG_VIDEO_SIGNAL_INFO targetVideoSignalInfo;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_DESKTOP_IMAGE_INFO
    {
        public POINTL PathSourceSize;
        public RECTL DesktopImageRegion;
        public RECTL DesktopImageClip;
    }

    [StructLayout(LayoutKind.Explicit, Pack = 1)]
    public struct DISPLAYCONFIG_MODE_INFO
    {
        [FieldOffset(0)]
        public DISPLAYCONFIG_MODE_INFO_TYPE infoType;
        [FieldOffset(4)]
        public UInt32 id;
        [FieldOffset(8)]
        public LUID adapterId;
        [FieldOffset(16)]
        public DISPLAYCONFIG_TARGET_MODE targetMode;
        [FieldOffset(16)]
        public DISPLAYCONFIG_SOURCE_MODE sourceMode;
        [FieldOffset(16)]
        public DISPLAYCONFIG_DESKTOP_IMAGE_INFO desktopImageInfo;
    }

    public enum DISPLAYCONFIG_DEVICE_INFO_TYPE
    {
        DISPLAYCONFIG_DEVICE_INFO_GET_SOURCE_NAME = 1,
        DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME = 2,
        DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_PREFERRED_MODE = 3,
        DISPLAYCONFIG_DEVICE_INFO_GET_ADAPTER_NAME = 4,
        DISPLAYCONFIG_DEVICE_INFO_SET_TARGET_PERSISTENCE = 5,
        DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_BASE_TYPE = 6,
        DISPLAYCONFIG_DEVICE_INFO_GET_SUPPORT_VIRTUAL_RESOLUTION = 7,
        DISPLAYCONFIG_DEVICE_INFO_SET_SUPPORT_VIRTUAL_RESOLUTION = 8,
        DISPLAYCONFIG_DEVICE_INFO_GET_ADVANCED_COLOR_INFO = 9,
        DISPLAYCONFIG_DEVICE_INFO_SET_ADVANCED_COLOR_STATE = 10,
        DISPLAYCONFIG_DEVICE_INFO_GET_SDR_WHITE_LEVEL = 11
        // DISPLAYCONFIG_DEVICE_INFO_FORCE_UINT32 = 0xFFFFFFFF
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DISPLAYCONFIG_DEVICE_INFO_HEADER
    {
        public DISPLAYCONFIG_DEVICE_INFO_TYPE type;
        public UInt32 size;
        public LUID adapterId;
        public UInt32 id;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct DISPLAYCONFIG_SOURCE_DEVICE_NAME
    {
        public DISPLAYCONFIG_DEVICE_INFO_HEADER header;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string viewGdiDeviceName;
    }

    public enum QDCFlags
    {
        QDC_ALL_PATHS = 0x00000001,
        QDC_ONLY_ACTIVE_PATHS = 0x00000002,
        QDC_DATABASE_CURRENT = 0x00000004,
        QDC_VIRTUAL_MODE_AWARE = 0x00000010,
        QDC_INCLUDE_HMD = 0x00000020
    }

    public enum DISPLAYCONFIG_TOPOLOGY_ID
    {
        DISPLAYCONFIG_TOPOLOGY_INTERNAL,
        DISPLAYCONFIG_TOPOLOGY_CLONE,
        DISPLAYCONFIG_TOPOLOGY_EXTEND,
        DISPLAYCONFIG_TOPOLOGY_EXTERNAL,
        DISPLAYCONFIG_TOPOLOGY_FORCE_UINT32
    }
    #endregion

    #region icm.h
    public enum DeviceClass : uint
    {
        CLASS_MONITOR    = 0x6d6e7472, //'mntr'
        CLASS_PRINTER    = 0x70727472, //'prtr'
        CLASS_SCANNER    = 0x73636e72, //'scnr'
        CLASS_LINK       = 0x6c696e6b, //'link'
        CLASS_ABSTRACT   = 0x61627374, //'abst'
        CLASS_COLORSPACE = 0x73706163, //'spac'
        CLASS_NAMED      = 0x6e6d636c  //'nmcl'
    }

    public enum COLORPROFILETYPE
    {
        CPT_ICC,
        CPT_DMP,
        CPT_CAMP,
        CPT_GMMP
    };

    public enum COLORPROFILESUBTYPE
    {
        // intent
        CPST_PERCEPTUAL = 0, //INTENT_PERCEPTUAL
        CPST_RELATIVE_COLORIMETRIC = 1, //INTENT_RELATIVE_COLORIMETRIC
        CPST_SATURATION = 2, //INTENT_SATURATION
        CPST_ABSOLUTE_COLORIMETRIC = 3, //INTENT_ABSOLUTE_COLORIMETRIC

        // working space
        CPST_NONE,
        CPST_RGB_WORKING_SPACE,
        CPST_CUSTOM_WORKING_SPACE,

        // advanced color aware
        CPST_STANDARD_DISPLAY_COLOR_MODE,
        CPST_EXTENDED_DISPLAY_COLOR_MODE
    };

    public enum WCS_PROFILE_MANAGEMENT_SCOPE
    {
        WCS_PROFILE_MANAGEMENT_SCOPE_SYSTEM_WIDE,
        WCS_PROFILE_MANAGEMENT_SCOPE_CURRENT_USER
    };

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DisplayID
    {
        LUID targetAdapterID;
        UInt32 sourceInfoID;
    }
    #endregion

    class NativeMethods
    {
        [DllImport("kernel32.dll")]
        public extern static bool LocalFree(IntPtr hMem);
        [DllImport("gdi32.dll")]
        public extern static IntPtr CreateIC(string lpszDriver, string lpszDevice, IntPtr lpszOutput, IntPtr lpInitData);
        [DllImport("gdi32.dll")]
        public extern static bool DeleteDC(IntPtr hDC);
        [DllImport("user32.dll")]
        public extern static IntPtr MonitorFromWindow(IntPtr hwnd, MONITORFROMWINDOWFLAGS dwFlags);
        [DllImport("user32.dll")]
        public extern static bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFOEX lpmi);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern bool EnumDisplayDevices(string lpDevice, uint iDevNum, ref DISPLAY_DEVICE lpDisplayDevice, uint dwFlags);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetDisplayConfigBufferSizes(QDCFlags flags, out UInt32 numPathArrayElements, out UInt32 numModeInfoArrayElements);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int QueryDisplayConfig(QDCFlags flags, ref UInt32 numPathArrayElements, [In, Out][MarshalAs(UnmanagedType.LPArray)] DISPLAYCONFIG_PATH_INFO[] pathArray, ref UInt32 numModeInfoArrayElements, [In, Out][MarshalAs(UnmanagedType.LPArray)] DISPLAYCONFIG_MODE_INFO[] modeArray, IntPtr currentTopologyId);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int DisplayConfigGetDeviceInfo(ref DISPLAYCONFIG_SOURCE_DEVICE_NAME requestPacket);
        [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
        public static extern bool GetICMProfile(IntPtr hDC, out int lpcbName, StringBuilder lpFileName);
        [DllImport("mscms.dll", CharSet = CharSet.Unicode)]
        public static extern bool WcsGetDefaultColorProfileSize(WCS_PROFILE_MANAGEMENT_SCOPE scope, [MarshalAs(UnmanagedType.LPTStr)]string deviceName, COLORPROFILETYPE colorProfileType, COLORPROFILESUBTYPE colorProfileSubType, UInt32 dwProfileID, out UInt32 cbProfileName);
        [DllImport("mscms.dll", CharSet = CharSet.Unicode)]
        public static extern bool WcsGetDefaultColorProfile(WCS_PROFILE_MANAGEMENT_SCOPE scope, [MarshalAs(UnmanagedType.LPTStr)]string deviceName, COLORPROFILETYPE colorProfileType, COLORPROFILESUBTYPE colorProfileSubType, UInt32 dwProfileID, UInt32 cbProfileName, IntPtr pProfileName);
        [DllImport("mscms.dll", CharSet = CharSet.Unicode)]
        public static extern bool WcsGetUsePerUserProfiles([MarshalAs(UnmanagedType.LPTStr)] string deviceName, DeviceClass deviceClass, out bool usePerUserProfiles);
        [DllImport("mscms.dll")]
        public static extern int ColorProfileGetDisplayUserScope(LUID targetAdapterID, UInt32 sourceID, out WCS_PROFILE_MANAGEMENT_SCOPE scope);
        [DllImport("mscms.dll")]
        public static extern int ColorProfileGetDisplayDefault(WCS_PROFILE_MANAGEMENT_SCOPE scope, LUID targetAdapterID, UInt32 sourceID, COLORPROFILETYPE profileType, COLORPROFILESUBTYPE profileSubType, out IntPtr profileName);
    }
}
